import random

def modular_sqrt(a, p):
    """ Find a quadratic residue (mod p) of 'a'. p
        must be an odd prime.

        Solve the congruence of the form:
            x^2 = a (mod p)
        And returns x. Note that p - x is also a root.

        0 is returned is no square root exists for
        these a and p.
        The Tonelli-Shanks algorithm is used (except
        for some simple cases in which the solution
        is known from an identity). This algorithm

        runs in polynomial time (unless the
        generalized Riemann hypothesis is false).
    """
    # Simple cases
    #
    if legendre_symbol(a, p) != 1:
        return 0
    elif a == 0:
        return 0
    elif p == 2:
        return p
    elif p % 4 == 3:
        return pow(a, (p + 1) // 4, p)

    # Partition p-1 to s * 2^e for an odd s (i.e.
    # reduce all the powers of 2 from p-1)
    #
    s = p - 1
    e = 0
    while s % 2 == 0:
        s //= 2
        e += 1

    # Find some 'n' with a legendre symbol n|p = -1.
    # Shouldn't take long.
    #
    n = 2
    while legendre_symbol(n, p) != -1:
        n += 1

    # Here be dragons!
    # Read the paper "Square roots from 1; 24, 51,
    # 10 to Dan Shanks" by Ezra Brown for more
    # information
    #

    # x is a guess of the square root that gets better
    # with each iteration.
    # b is the "fudge factor" - by how much we're off
    # with the guess. The invariant x^2 = ab (mod p)
    # is maintained throughout the loop.
    # g is used for successive powers of n to update
    # both a and b
    # r is the exponent - decreases with each update
    #
    x = pow(a, (s + 1) // 2, p)
    b = pow(a, s, p)
    g = pow(n, s, p)
    r = e

    while True:
        t = b
        m = 0
        for m in range(r):
            if t == 1:
                break
            t = pow(t, 2, p)

        if m == 0:
            return x

        gs = pow(g, 2 ** (r - m - 1), p)
        g = (gs * gs) % p
        x = (x * gs) % p
        b = (b * g) % p
        r = m


def legendre_symbol(a, p):
    """ Compute the Legendre symbol a|p using
        Euler's criterion. p is a prime, a is
        relatively prime to p (if p divides
        a, then a|p = 0)

        Returns 1 if a has a square root modulo
        p, -1 otherwise.
    """
    ls = pow(a, (p - 1) // 2, p)
    return -1 if ls == p - 1 else ls

def eea(i, j):
    assert(isinstance(i, int) or isinstance(i, long))
    assert(isinstance(j, int) or isinstance(i, long))
    (s, t, u, v) = (1, 0, 0, 1)
    while j != 0:
        (q, r) = (i // j, i % j)
        (unew, vnew) = (s, t)
        s = u - (q * s)
        t = v - (q * t)
        (i, j) = (j, r)
        (u, v) = (unew, vnew)
    (d, m, n) = (i, u, v)
    return (d, m, n)

def modinv(n, p):
    return eea(p, n)[1] % p

class ecc(object):

    class _jacob_point(object):

        def __init__(self, x, y, z, ecc):
            self.isinfinity = (z == None)

            self.x = x
            self.y = y
            self.z = z
            self.ecc = ecc

        def __add__(self, other):
            '''
            Really useful reference:
            https://eprint.iacr.org/2008/060.pdf
            '''
            assert isinstance(other, ecc._jacob_point)

            if self.isinfinity:
                return other
            elif other.isinfinity:
                return self

            if self.x == other.x and self.y == other.y and self.z == other.z:
                '''
                Point doubling
                '''
                S = (4 * self.x * self.y ** 2) % self.ecc.p
                M = (3 * self.x**2 + self.ecc.a * self.z**4) % self.ecc.p
                r_x = (M**2 - 2*S) % self.ecc.p
                r_y = (M * (S - r_x) - 8 * self.y**4) % self.ecc.p
                r_z = (2 * self.y * self.z) % self.ecc.p
            else:
                '''
                Point addition
                '''
                U1 = (self.x * other.z**2) % self.ecc.p
                U2 = (other.x * self.z**2) % self.ecc.p
                S1 = (self.y * other.z**3) % self.ecc.p
                S2 = (other.y * self.z**3) % self.ecc.p

                X = (U2 - U1) % self.ecc.p
                Y = (S2 - S1) % self.ecc.p
                r_x = (-X**3 - 2*U1*X**2 + Y**2) % self.ecc.p
                r_y = (-S1*X**3 + Y*(U1*X**2 - r_x)) % self.ecc.p
                r_z = (X * self.z * other.z) % self.ecc.p

            return ecc._jacob_point(r_x, r_y, r_z, self.ecc)

        def __mul__(self, scalar):
            assert isinstance(scalar, int) or isinstance(scalar, long)
            N = self
            R = ecc._jacob_point(None, None, None, self.ecc)
            for x in range(scalar.bit_length()):
                if ((1 << x) & scalar) > 0: #test the bit
                    R = R + N
                N = N + N

            return R

        def valid(self):
            return self.y ** 2 % self.ecc.p == \
                self.x ** 3 - 3*self.x*self.z**4 + self.ecc.b * self.z**6

        def to_affine_coordinates(self):
            if self.z == 0:
                return ecc.infinity

            x = (self.x * modinv(self.z**2, self.ecc.p)) % self.ecc.p
            y = (self.y * modinv(self.z**3, self.ecc.p)) % self.ecc.p
            return ecc._point(x, y, self.ecc)

        def __str__(self):
            return "<Point ({}, {}, {})>".format(self.x, self.y, self.z)

    class _point(object):

        def __init__(self, x, y, ecc):
            self.isinfinity = (x is None and y is None and ecc is None)

            self.x = x
            self.y = y
            self.ecc = ecc

        def __add__(self, point):
            assert isinstance(point, ecc._point)

            if point.isinfinity:
                return self
            elif self.isinfinity:
                return point
            else:
                assert self.ecc == point.ecc

            if self.x == point.x and self.y != point.y:
                return ecc.infinity

            if self.x == point.x and self.y == point.y:
                '''
                Point doubling
                '''
                s = (3 * (self.x ** 2) + self.ecc.a) * modinv(2 * self.y, self.ecc.p) % self.ecc.p
            else:
                delta_y = (self.y - point.y) % self.ecc.p
                delta_x = (self.x - point.x) % self.ecc.p
                s = (delta_y * modinv(delta_x, self.ecc.p)) % self.ecc.p

            r_x = (s ** 2 - self.x - point.x) % self.ecc.p
            r_y = (s * (self.x - r_x) - self.y) % self.ecc.p

            return ecc._point(r_x, r_y, self.ecc)

        def __mul__(self, scalar):
            assert isinstance(scalar, int) or isinstance(scalar, long)

            N = self
            R = ecc.infinity
            for x in range(scalar.bit_length()):
                if ((1 << x) & scalar) > 0: #test the bit
                    R = R + N
                N = N + N

            return R

        def __rmul__(self, scalar):
            return self.__mul__(scalar)

        def __str__(self):
            if self.isinfinity:
                return "< Point infinity >"
            return "< Point ({}, {})>".format(self.x, self.y)

        def __eq__(self, other):
            if (self.isinfinity and other._isinfinity):
                return True

            return self.x == other.x and self.y == other.y and\
                    self.ecc == other.ecc

        def valid(self):
            if self.isinfinity:
                return True
            return ((self.y ** 2) % self.ecc.p) ==\
                (self.x ** 3 + (self.ecc.a * self.x) + self.ecc.b) % self.ecc.p

        def to_jacobian_coordinates(self):
            '''
            Converting to Jacobian Projective coordinate
            '''
            sjx = self.x
            sjy = self.y
            sjz = 1

            return ecc._jacob_point(sjx, sjy, sjz, self.ecc)

    infinity = _point(None, None, None)

    def __init__(self, a, b, p, order=None):
        if ((4 * a**3 + 27*b**3) % p) == 0:
            raise Exception("The curve has singularities")

        self.a = a
        self.b = b
        self.p = p

    def point(self, x, parity):
        '''
        if self.p % 4 == 3:
            def min(a, b):
                return a if a < b else b

            y2 = (x ** 3 + self.a * x + self.b)
            yl = pow(y2, (self.p + 1) // 4, self.p)
            return ecc._point(x, min(yl, self.p - yl), self)
        '''
        alpha = (x ** 3 + self.a * x + self.b) % self.p

        b1 = modular_sqrt(alpha, self.p)
        if b1 == 0:
            raise Exception("Invalid x coordinate: x = {}".format(x))
        b2 = self.p - b1

        if b1 % 2 == 1:
            temp = b1
            b1 = b2
            b2 = temp

        if parity:
            return ecc._point(x, b2, self)
        else:
            return ecc._point(x, b1, self)

    def miller(self, P, Q):
        '''
        See https://crypto.stanford.edu/pbc/notes/ep/miller.html
        '''
        def g(P, Q, T):
            if (P.x, P.y) == (Q.x, Q.y):
                #tangent
                return (-3 * P.x ** 2 + self.a) * T.x + (2 * P.y) * T.y + \
                        (-2 * P.y**2 + P.x * (3 * P.x**2 + self.a))
            elif P.y == (-Q.y % self.p):
                #vertical
                return T.x - P.x
            else:
                #line
                return (P.y - Q.y) * T.x + (P.x - Q.x) * T.y + (P.x*Q.y - Q.x*P.y)

        assert self.order is not None

        f = 1
        V = P
        for i in reversed(range(self.order.bit_length())):
            V2 = 2 * V
            V2n = self.point(V2.x, True if V2.y % 2 == 0 else False)
            f = f**2 * g(V, V, Q) * modinv(g(V2, V2n, Q), self.order)
            V = V2
            if 1 << i & self.order > 0:
                VP = V + P
                assert VP.valid()
                VPn = self.point(VP.x, True if V2.y % 2 == 0 else False)
                f = f * g(V, P, Q) * modinv(g(VP, VPn, Q), self.order)
                V = VP

        return f

    def tate_pairing(self, P, Q):
        return pow(self.miller(P, Q), ((self.p**2) - 1) * modinv(self.order, self.order - 1),  self.order)

    def weil_pairing(self, P, Q):
        return self.miller(P, Q) * modinv(self.miller(Q, P), self.order) % self.order



simple_curve = ecc(2, 2, 17)
simple_curve.generator = simple_curve.point(3, True)
simple_curve.order = 19
'''
Standard curves
'''
#FIXME(mauricio): check generators
secp112r1 = ecc(int("00db7c2abf62e35e668076bead2088", 16), #a
            int("659ef8ba043916eede8911702b22", 16), #b
            int("00db7c2abf62e35e668076bead208b", 16)) #p
secp112r1.generator = secp112r1.point(int("09487239995a5ee76b55f9c2f098", 16),
                True)
secp112r1.order = int("db7c2abf62e35e7628dfac6561c5", 16)

secp256k1 = ecc(0, 7, int("fffffffffffffffffffffffffffffffffffffffffffffffffffffffefffffc2f", 16))
secp256k1.generator = secp256k1.point(int("79be667ef9dcbbac55a06295ce870b07029bfcdb2dce28d959f2815b16f81798", 16), True)
secp256k1.order = int("fffffffffffffffffffffffffffffffebaaedce6af48a03bbfd25e8cd0364141", 16)

secp384r1 = ecc(int("fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffeffffffff0000000000000000fffffffc", 16),
                int("00b3312fa7e23ee7e4988e056be3f82d19181d9c6efe8141120314088f5013875ac656398d8a2ed19d2a85c8edd3ec2aef", 16),
                int("fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffeffffffff0000000000000000ffffffff", 16))
secp384r1.generator = secp384r1.point(int("aa87ca22be8b05378eb1c71ef320ad746e1d3b628ba79b9859f741e082542a385502f25dbf55296c3a545e3872760ab7", 16), True)
secp384r1.order = int("ffffffffffffffffffffffffffffffffffffffffffffffffc7634d81f4372ddf581a0db248b0a77aecec196accc52973", 16)

#secp521k1 = ecc(a, b, p)
#secp521k1.generator = secp521k1.point(x, parity)

#prime256v1 = ecc(a, b, p)
#prime256v1.generator = prime256v1.point(x, parity)

def signing():
    myecc = ecc(2, 2, 17)
    G = myecc.point(3, False)
    n = 19 # order of the curve

    invalid_rs = 0
    invalid_ss = 0
    valid_sigantures = 0

    for dA in range(n):
        for e in range(n):
            for k in range(1, n):
                '''
                Keygen
                '''
                #dA = 1
                Qa = G * dA

                '''
                Signing
                '''
                #e = 1
                #k = 1
                #print("k: {}, dA: {}, e: {}".format(k, dA, e))
                R = G * k
                r = R._x

                if r == 0:
                    invalid_rs += 1
                    continue

                """
                    e + dA * r
                s = ----------
                        k
                """
                s = ((e + (dA * r))  * modinv(k, n)) % n
                if s == 0:
                    invalid_ss += 1
                    continue


                signature = (r, s)
                #print("Signature r={}, s={}".format(signature[0], signature[1]))

                '''
                Verification
                                k
                w = s^-1 = -----------
                            e + dA * r

                                e * k
                u1 = e * w = ----------
                             e + dA * r

                                r * k
                u2 = r * w = ----------
                             e + dA * r

                             u1G + u2Qa = u1G + u2dAG = rG
                '''
                w = modinv(signature[1], n)
                u1 = e * w % n
                u2 = signature[0] * w % n
                P1 = (G * u1)
                P2 = (Qa * u2)
                P = P1 + P2

                if signature[0] == P._x:
                    valid_sigantures += 1
                else:
                    print("Error: {}".format(signature))

    print("Total tests: {}".format(19 * 19 * 18))
    print("Valid: {}%".format(100.0 * valid_sigantures * 1.0 / (19 * 19 * 18)))
    print("Invalid r's: {}%".format(100.0 * invalid_rs * 1.0 / (19 * 19 * 18)))
    print("Invalid s's: {}%".format(100.0 * invalid_ss * 1.0 / (19 * 19 * 18)))

def key_agreement():
    myecc = ecc(2, 2, 17)
    p1 = myecc.point(10, True)

    secret = random.randint(1, 19)
    print("Secret: {}".format(secret))

    public_key =  p1 * secret
    print("public_key = {}: {}".format(public_key, public_key.valid()))

    session_seed = random.randint(1, 19)
    r = p1 * session_seed
    print("Sending r = {}".format(r))

    s = public_key * session_seed
    print("S = {}".format(s))

    another_s = r * secret
    print("S' = {}".format(another_s))


def validate_sum():
    myecc = ecc(2, 2, 17)

    points = []
    for x in range(0, 17):
        try:
            p1 = myecc.point(x, False)
            p2 = myecc.point(x, True)
            points.append(p1)
            print(p1)
            points.append(p2)
            print(p2)
        except:
            pass

    points.append(ecc.infinity)
    print(ecc.infinity)

    valids = 0
    for i in range(len(points)):
        for j in range(len(points)):
            r = points[i] + points[j]
            #print("{} + {} = {} (is valid: {})".format(points[i], points[j], r, r.valid()))
            if r.valid():
                valids += 1
            else:
                print("{} + {} = {} (is valid: {})".format(points[i], points[j], r, r.valid()))
                rcommute = points[j] + points[i]
                print("{} + {} = {} (is valid: {})".format(points[j], points[i], rcommute, rcommute.valid()))

    print("Valids: {} of {} ({} %)".format(valids, len(points)**2, 100 * valids * 1.0 / len(points)**2))

def jacobian_projection():
    myecc = ecc(2, 2, 17)
    G = myecc.point(0, False)
    GJ = G.to_jacobian_coordinates()
    #n = 19 # order of the curve

    print("G = {}".format(G))
    print("G + G + G = {}".format(G + G + G + G))
    print("G * 4 = {}".format(G * 4))

    S4GJ = GJ + GJ + GJ + GJ
    print("GJ + GJ + GJ + GJ = {}, to affine = {}".format(S4GJ, S4GJ.to_affine_coordinates()))
    GJ4 = GJ * 4
    print("GJ * 4 = {}, to affine = {}".format(GJ4, GJ4.to_affine_coordinates()))

def stress_test():

    curves = [secp112r1, secp256k1, secp384r1]
    for ecc in curves:
        g = ecc.generator

        assert g.valid()
        print("G is \n\t{},\n and valid? \n\t{}".format(g, g.valid()))

        print("G * order: {}".format(g * ecc.order))

        x = random.randint(1, int("db7c2abf62e35e7628dfac6561c5", 16))
        Qa = g.to_jacobian_coordinates() * x
        #Qa = Qa.to_affine_coordinates()
        #assert Qa.valid()
        print("x is {}. Qa is \n\t{}".format(x, Qa))

        y = random.randint(1, int("db7c2abf62e35e7628dfac6561c5", 16))
        Qb = g.to_jacobian_coordinates() * y
        #Qb = Qb.to_affine_coordinates()
        #assert Qb.valid()
        print("y is {}. Qb is \n\t{}".format(y, Qb))

        SSa = Qb * x
        SSb = Qa * y
        SSaa = SSa.to_affine_coordinates()
        SSba = SSb.to_affine_coordinates()
        print("SSa is \n\t{},\n to affine, \n\t{}".format(SSa, SSaa))
        print("SSb is \n\t{},\n to affine, \n\t{}".format(SSb, SSba))

        assert  SSaa == SSba

def tate_pairing():
    G = simple_curve.generator
    tate_right = 0
    weil_right = 0
    combinations = 0
    for i in range(1, simple_curve.order - 1):
        for j in range(1, simple_curve.order - i):
            combinations += 1
            #print("{} \t+\t{} =\t{}".format(G * i, G * j, G * i + G * j))
            tate_val = simple_curve.tate_pairing(G * i, G * j)
            tate_2val = simple_curve.tate_pairing(G, G * (j + i))
            if tate_val == tate_2val:
                tate_right += 1
            else:
                print("tate({},\t{})\t=\t{}".format(G * i, G * j, tate_val))
                print("tate({},\t{})\t=\t{}".format(G, G * (j + i), tate_2val))
            weil_val = simple_curve.weil_pairing(G * i, G * j)
            weil_2val = simple_curve.weil_pairing(G, G * (j + i))
            if weil_val == weil_2val:
                weil_right += 1
            else:
                print("weil({},\t{})\t=\t{}".format(G * i, G * j, weil_val))
                print("weil({},\t{})\t=\t{}".format(G, G * (j + i), weil_2val))


    print("tate: {}%".format(tate_right / combinations * 100.0))
    print("weil: {}%".format(weil_right / combinations * 100.0))



if __name__ == "__main__":
    #validate_sum()
    #key_agreement()
    #signing()
    #jacobian_projection()
    #stress_test()
    tate_pairing()
