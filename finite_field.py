def extendedEuclideanAlgorithm(a, p, zero=0, one=1):
    t, newt = zero, one
    r, newr = p, a
    quotient = zero
    while newr != zero:
        quotient = r // newr
        (r, newr) = (newr, r - (quotient * newr))
        (t, newt) = (newt, t - (quotient * newt))
    return t, r, quotient

def modinv(n, p, zero=0, one=1):
    """
    Calculates the modular inverse of n mod p, thus returning n^1
    so that n * n^-1 = 1 (mod p)

    Args:
        n (any): The element to search for the inverse.
        p (any): The element that defines the fields.
        zero (any): The sum identity in the field. Defaults to 0.
        one (any): The multiplication identity in the field. Defaults to 1.
    Returns:
        any: The inverse multiplicative of n over p
    """
    assert n.__class__ == p.__class__ == zero.__class__ == one.__class__
    x, _, _ = extendedEuclideanAlgorithm(n, p, zero, one)
    return x

def gcd(a, b, zero=0, one=1):
    """
    Calculates the greatest common divisor between ``a`` and ``b``.
    Args:
        a (any): One of the elements to calculate the gcd.
        b (any): The other element to calculate the gcd.
        zero (any): The sum identity in the field. Defaults to 0.
        one (any): The multiplication identity in the field. Defaults to 1.
    Returns:
        any: The greatest common divisor between ``a`` and ``b``.
    """
    if abs(a) < abs(b):
        return gcd(b, a, zero, one)

    while b != zero:
        q,r = divmod(a,b)
        a,b = b,r

    return a

class finite_field_element(object):
    """
    This class represents a element of a Finite Field.
    """

    def __init__(self, e, finite_field, zero=None, one=None):
        self.finite_field = finite_field
        self.e = e % finite_field.p
        self.zero = zero
        self.one = one

    def inverse(self):
        inverse = modinv(self.e, self.finite_field.p, self.zero.e, self.one.e)
        return finite_field_element(inverse, self.finite_field, self.zero, self.one)

    def __str__(self):
        return "<{}>".format(str(self.e), str(self.finite_field.p))

    def __repr__(self):
        return "<{}>".format(str(self.e), str(self.finite_field.p))

    def __eq__(self, other):
        assert isinstance(other, finite_field_element)
        return self.e == other.e

    def __ne__(self, other):
        return not self.__eq__(other)

    def __abs__(self):
        return abs(self.e)

    def __neg__(self):
        return finite_field_element(-self.e, self.finite_field, self.zero, self.one)

    def __add__(self, other):
        assert isinstance(other, finite_field_element)
        assert self.finite_field == other.finite_field
        return finite_field_element(self.e + other.e, self.finite_field, self.zero, self.one)

    def __sub__(self, other):
        assert isinstance(other, finite_field_element)
        assert self.finite_field == other.finite_field
        return finite_field_element(self.e - other.e, self.finite_field, self.zero, self.one)

    def __mul__(self, other):
        assert isinstance(other, finite_field_element)
        assert self.finite_field == other.finite_field
        return finite_field_element(self.e * other.e, self.finite_field, self.zero, self.one)

    def __div__(self, other):
        assert isinstance(other, finite_field_element)
        assert self.finite_field == other.finite_field
        return finite_field_element(self.e * other.inverse().e, self.finite_field, self.zero, self.one)

    def __floordiv__(self, other):
        assert isinstance(other, finite_field_element)
        assert self.finite_field == other.finite_field
        return finite_field_element(self.e * other.inverse().e, self.finite_field, self.zero, self.one)

    def __truediv__(self, other):
        assert isinstance(other, finite_field_element)
        assert self.finite_field == other.finite_field
        return finite_field_element(self.e * other.inverse().e, self.finite_field, self.zero, self.one)

    def __rdiv__(self, other):
        assert isinstance(other, finite_field_element)
        assert self.finite_field == other.finite_field
        return finite_field_element(self.inverse().e * other.e, self.finite_field, self.zero, self.one)

    def __rtruediv__(self, other):
        assert isinstance(other, finite_field_element)
        assert self.finite_field == other.finite_field
        return finite_field_element(self.inverse().e * other.e, self.finite_field, self.zero, self.one)

    def __rfloordiv__(self, other):
        assert isinstance(other, finite_field_element)
        assert self.finite_field == other.finite_field
        return finite_field_element(self.inverse().e * other.e, self.finite_field, self.zero, self.one)

    def __pow__(self, other):
        assert isinstance(other, int) or isinstance(other, long)
        return finite_field_element(self.e ** other, self.finite_field, self.zero, self.one)


class finite_field(object):
    """
    This class represents a Finite Field.
    """
    def __init__(self, p, zero=0, one=1):
        """
        Creates a Finite Field. The p is a "prime" element and defines the Finite Field.
        Parameters ``zero`` and ``one`` are the identities for sum and multiplication operations
        in this field respectively.

        To obtain elements inside this field just call this instance with an element on the
        set of elements that this Finite Field is built over.

        Args:
            p (any): The "prime" that defines the field.
            zero (any): The sum identity element for the field.
            one (any): The multiplication identity element for the field.
        """
        self.p = p
        self.zero = finite_field_element(zero, self, None, None)
        self.one = finite_field_element(one, self, None, None)
        self.zero.zero = self.zero
        self.zero.one = self.one
        self.one.zero = self.zero
        self.one.one = self.one

    def __call__(self, e):
        return finite_field_element(e, self, self.zero, self.one)



class polynomial(object):
    """
    Represents a polynomial. This class implements all the operations
    that one can do with polinomials.
    """

    def __init__(self, coeffs):
        """
        Args:
            coeffs(list): The list of coeffs of this polynomial. The list is
                interpreted in order, or in other words, the index of the
                coefficient corresponds to its degree in the polynomial.
        """
        assert isinstance(coeffs, dict)
        assert len(coeffs.keys()) > 0
        zero = 0
        if hasattr(coeffs[coeffs.keys()[-1]], 'zero'):
            zero = coeffs[coeffs.keys()[-1]].zero
        self.coeffs = {k: v for k, v in coeffs.items() if v != zero}


    def degree(self):
        """
        Returns:
            int: The degree of the polynomial.
        """
        if self.is_zero():
            return 1
        return max(self.coeffs.keys())

    def is_zero(self):
        """
        Returns:
            bool: True if this polynomial corresponds to zero.
        """
        return len(self.coeffs) == 0

    def __pretty_print(self, i):
        zero = 0
        if hasattr(self.coeffs[i], 'zero'):
            zero = self.coeffs[i].zero
        if self.coeffs[i] != zero:
            if i == 0:
                return "{}".format(self.coeffs[i])
            else:
                return "{}x^{}".format(self.coeffs[i], i)


    def __str__(self):
        zero = 0
        if hasattr(self.coeffs[self.coeffs.keys()[-1]], 'zero'):
            zero = self.coeffs[self.coeffs.keys()[-1]].zero

        if self.is_zero():
            return str(zero)

        l = list(reversed([self.__pretty_print(i) for i in self.coeffs]))
        return "+".join([a for a in l if a is not None])

    def __eq__(self, other):
        assert isinstance(other, polynomial)
        if len(self.coeffs) != len(other.coeffs):
            return False
        return all(a == b and i == j for (i, a), (j, b) in zip(self.coeffs, other.coeffs))

    def __ne__(self, other):
        return not self.__eq__(other)

    def __abs__(self):
        return len(self.coeffs)

    def __repr__(self):
        return self.__str__()

    def __neg__(self):
        neg_coeffs = {}
        for i in self.coeffs.keys():
            neg_coeffs = -self.coeffs[i]
        return polynomial(neg_coeffs)

    def __add__(self, other):
        assert isinstance(other, polynomial)
        if self.is_zero():
            return other
        if other.is_zero():
            return self

        if hasattr(self.coeffs[self.coeffs.keys()[0]], "finite_field"):
            finite_field = self.coeffs[self.coeffs.keys()[0]].finite_field
        else:
            finite_field = lambda n: n
        zero = finite_field(0)

        sum_coeffs = {}
        other_keys = other.coeffs.keys()
        self_keys = self.coeffs.keys()
        for i in self_keys:
            sum_coeffs[i] = self.coeffs[i]
            if i in other_keys:
                sum_coeffs[i] += other.coeffs[i]

        sum_coeffs_keys = sum_coeffs.keys()
        for j in other_keys:
            if j not in sum_coeffs_keys:
                sum_coeffs[j] = other.coeffs[j]

        res = polynomial(sum_coeffs)
        return res

    def __sub__(self, other):
        assert isinstance(other, polynomial)
        if self.is_zero():
            return -other
        if other.is_zero():
            return self

        if hasattr(self.coeffs[self.coeffs.keys()[0]], "finite_field"):
            finite_field = self.coeffs[self.coeffs.keys()[0]].finite_field
        else:
            finite_field = lambda n: n
        zero = finite_field(0)

        sum_coeffs = {}
        other_keys = other.coeffs.keys()
        self_keys = self.coeffs.keys()
        for i in self_keys:
            sum_coeffs[i] = self.coeffs[i]
            if i in other_keys:
                sum_coeffs[i] -= other.coeffs[i]

        sum_coeffs_keys = sum_coeffs.keys()
        for j in other_keys:
            if j not in sum_coeffs_keys:
                sum_coeffs[j] = other.coeffs[j]
        res = polynomial(sum_coeffs)
        return res

    def __mul__(self, other):
        if hasattr(self.coeffs[self.coeffs.keys()[0]], "finite_field"):
            ff = self.coeffs[self.coeffs.keys()[0]].finite_field
        else:
            ff = lambda n: n

        #sanit checks of the parameters
        if isinstance(other, int):
            other = polynomial([ff(other)])
        elif isinstance(other, finite_field_element):
            assert other.finite_field == finite_field
            other = polynomial([other])

        assert isinstance(other, polynomial)

        zero = ff(0)
        if self.is_zero():
            return polynomial({0: zero})
        if other.is_zero():
            return polynomial({0: zero})

        mul_coeffs = {}

        for i in self.coeffs.keys():
            for j in other.coeffs.keys():
                if i + j in mul_coeffs.keys():
                    mul_coeffs[i + j] += self.coeffs[i] * other.coeffs[j]
                else:
                    mul_coeffs[i + j] = self.coeffs[i] * other.coeffs[j]

        res = polynomial(mul_coeffs)
        return res

    def __divmod__(self, divisor):
        assert isinstance(divisor, polynomial)
        if divisor.is_zero():
            raise Exception("Division by 0.")

        if hasattr(self.coeffs[self.coeffs.keys()[0]], "finite_field"):
            finite_field = self.coeffs[self.coeffs.keys()[0]].finite_field
        else:
            finite_field = lambda n: n

        quotient, remainder = polynomial({0: finite_field(0)}), self

        zero = finite_field(0)

        while remainder.degree() >= divisor.degree() and not remainder.is_zero():
            monomialExponent = remainder.degree() - divisor.degree()
            monomialDivisor = polynomial({monomialExponent: remainder.coeffs[remainder.coeffs.keys()[-1]] // divisor.coeffs[divisor.coeffs.keys()[-1]]})
            quotient = quotient + monomialDivisor
            remainder = remainder - (monomialDivisor * divisor)


        return quotient, remainder

    def __div__(self, divisor):
        return (self.__divmod__(divisor))[0]

    def __mod__(self, module):
        return (self.__divmod__(module))[1]

    def __truediv__(self, divisor):
        return (self.__divmod__(divisor))[0]

    def __floordiv__(self, divisor):
        return (self.__divmod__(divisor))[0]

    def __rdiv__(self, other):
        return other.__divmod__(self)[0]

    def __rtruediv__(self, other):
        return other.__divmod__(self)[0]

    def __pow__(self, other):
        """
        Does the power of a polynomial as follows in the example:
        (x^3 + x^1)^3 = (x^9 + x^3)
        """
        assert isinstance(other, int)

        zero = 0
        if hasattr(self.coeffs[self.coeffs.keys()[0]], 'zero'):
            zero = self.coeffs[self.coeffs.keys()[0]].zero

        temp_coeffs = {}
        for i in self.coeffs:
            temp_coeffs[i * other] = self.coeffs[i]

        res = polynomial(temp_coeffs)
        return res

    def is_irreducible(self):
        """
        Finds out if this polynomial is irreducible over its Finite Field or not.
        This function only work if this polynomial coefficients are Finite Field Elements.

        Returns:
            bool: True if this polynomial is irreducible
        """
        if not hasattr(self.coeffs[0], 'finite_field'):
            raise Exception("irreducible polynomials only can be calculated over finite fields.")

        finite_field = self.coeffs[0].finite_field
        zero = polynomial([finite_field(0)])
        one = polynomial([finite_field(1)])
        x = polynomial([finite_field(0), finite_field(1)])
        i = 1
        while self.degree() >= 2*i:
            test = (x ** (finite_field.p ** i) - x)
            g = gcd(self, test, zero, one)
            if g != one:
                return False
            i += 1
        return True

def find_irreducible(finite_field, degree):
    """
    Finds an irreducible polynomial of the disered degree on the Finite Field.
    Args:
        finite_field (finite_field): The Finite Field that will be used for the search
        degree (int): The desired degree of the polynomial, so one can create a Fq with
            q = p^degree.
    """
    def product(*args, **kwargs):
        pools = [tuple(pool) for pool in args] * kwargs.get('repeat', 1)
        maxis = [len(pool) for pool in pools]
        modules = maxis[:]
        for i in range(len(modules)-1):
            modules[i+1] *= modules[i]
        for i in range(modules[-1]):
            result = []
            temp = i
            for prod, pool in zip(maxis, pools):
                result += [pool[temp % prod]]
                temp = temp // prod
            yield result

    possible_coeffs = [finite_field(n) for n in range(finite_field.p)]

    for coeffs in product(possible_coeffs, repeat=degree):
        poly = polynomial(list(coeffs) + [finite_field(1)])
        if poly.is_irreducible():
            return poly

if __name__ == "__main__":
    z2 = finite_field(2, 0, 1)
    zero = polynomial([z2(0)])
    one = polynomial([z2(1)])

    print("=" * 37 + "Test 1" + "=" * 37)
    irred = polynomial([z2(1), z2(1), z2(0), z2(1), z2(1), z2(0), z2(0), z2(0), z2(1)])
    print("irred: {}".format(irred))
    Fp = finite_field(irred, zero, one)
    poly = polynomial([z2(1), z2(1), z2(0), z2(0), z2(1), z2(0), z2(1)])
    print("poly: {}".format(poly))
    a = Fp(poly)
    print("a (mod irred): {} (mod {})".format(a, irred))
    a1 = a.inverse()
    print("a^-1 (mod irred): {} (mod {})".format(a.inverse(), irred))
    print("1 = a * a^-1 (mod irred): {} = {} * {} (mod {})".format(a * a1, a, a1, irred))

    print("=" * 37 + "Test 2" + "=" * 37)
    irred = polynomial([z2(1), z2(1), z2(1)])
    poly = polynomial([z2(1),z2(1),z2(0),z2(1)])
    print("poly: {}".format(poly))
    print("irred: {}".format(irred))
    Fp = finite_field(irred, zero, one)
    a = Fp(poly)
    print("a (mod irred): {} (mod {})".format(a, irred))
    a1 = a.inverse()
    print("a^-1 (mod irred): {} (mod {})".format(a.inverse(), irred))
    print("1 = a * a^-1 (mod irred): {} = {} * {} (mod {})".format(a * a1, a, a1, irred))

    print("=" * 37 + "Test 3" + "=" * 37)
    z3 = finite_field(3, 0, 1)
    zero = polynomial([z3(0)])
    one = polynomial([z3(1)])
    '''
    Sage irreducibles for F3^n | n E [2, 9]
    x^2 + 2*x + 2
    x^3 + 2*x + 1
    x^4 + 2*x^3 + 2
    x^5 + 2*x + 1
    x^6 + 2*x^4 + x^2 + 2*x + 2
    x^7 + 2*x^2 + 1
    x^8 + 2*x^5 + x^4 + 2*x^2 + 2*x + 2
    x^9 + 2*x^3 + 2*x^2 + x + 1
    '''
    irred = polynomial([z3(1), z3(2), z3(0), z3(1)]) #x^3 + 2x + 1
    #irred = find_irreducible(z3, 3)
    print("is irreducible ({})? {}".format(irred, irred.is_irreducible()))
    poly = polynomial([z3(2),z3(1)]) #x + 2
    print("poly: {}".format(poly))
    print("irred: {}".format(irred))
    Fp = finite_field(irred, zero, one)
    a = Fp(poly)
    print("a (mod irred): {} (mod {})".format(a, irred))
    a1 = a.inverse() # should be 2*x^2 + 2*x
    print("a^-1 (mod irred): {} (mod {})".format(a.inverse(), irred))
    print("1 = a * a^-1 (mod irred): {} = {} * {} (mod {})".format(a * a1, a, a1, irred))
    print("=" * 37 + "Test 4" + "=" * 37)
    z3 = finite_field(3, 0, 1)
    zero = polynomial([z3(0)])
    one = polynomial([z3(1)])
    irred = find_irreducible(z3, 12)
    print("irred: {}".format(irred))
