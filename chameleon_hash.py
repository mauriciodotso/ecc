import hashlib
import ecc
import random

class ecc_chameleon_hash(object):

    def __init__(self, ecc, public_key, hash_function=None):
        '''
        :param:ecc: Contains the elliptic curve parameters
        :param:public_key: Is the public key to be used with this hash instance
        :param:hash_function: Contains the reference to the classic hash function
            that should be used in this chameleon hash
        '''

        self.ecc = ecc
        self.public_key = public_key
        if hash_function is not None:
            self.hash_function = hash_function
        else:
            self.hash_function = hashlib.sha256

    def digest(self, message):
        '''
        Digests the :param:message using this :self:public_key

        :param:message: The message to be digested
        :returns: The resulting hash tuple of the algorithm. This tuple
        looks as (g^r, hg^e, g^H(m)(hg^e)^r).
        '''
        G = self.ecc.generator.to_jacobian_coordinates()

        r = random.randint(1, self.ecc.order)
        e = random.randint(1, self.ecc.order)
        h = self.hash_function(message.encode('utf-8')).hexdigest()
        h = int(h, 16)

        Qae = self.public_key * e
        H = Qae * (r + h)

        return (G * r, Qae, H)

    def collide(self, hash_tuple, new_message, private_key):
        '''
        Collides a hash_tuple outputting a new g^r that verifies with the
        :param:new_message

        :param:hash_tuple: The tuple used as hash value. It has the form
        (g^r, hg^e, g^H(m)(hg^e)^r).
        :param:new_message: The new message that the owner of the key desires to
        collide the hash
        :param:private_key: The private key of the owner that is going to be used
        to compute the collision

        :returns: The resulting hash tuple of the algorithm. This tuple
        looks as (g^r', hg^e, g^H(m')(hg^e)^r')
        '''
        pass

    def verify(self, hash_tuple, message):
        '''
        Checks wheter the :param:hash_tuple is a valid hash or not.

        :param:hash_tuple: The tuple used as hash value. It has the form
        (g^r, hg^e, g^H(m)(hg^e)^r).
        '''
        G = self.ecc.generator.to_jacobian_coordinates()
        h = self.hash_function(message.encode('utf-8')).hexdigest()
        h = int(h, 16)

        Gr, Qae, H = hash_tuple
        Hh = H + (G * ecc.modinv(h, self.ecc.order))

        return self.ecc.tate_pairing(Gr, Qae) == self.ecc.tate_pairing(G, Hh)

    def precollide(self, hash_value, message, private_key):
        '''
        Generates a tuple such that the elements in the tuple are in the form
        (g^r, hg^e, hash_value). Or in other words, use the hash_value as the result
        of a valid hash. The generated tuple is going to be a valid one.

        :param:hash_value: The hash value that the owner choose.
        :param:message: The message that is going to be used in the collision
        :param:private_key: The key of the owner.
        :returns: A valid hash value tuple in the form (g^r, hg^e, hash_value).
        '''
        pass


def test_chameleon_hash():
    x = random.randint(2, ecc.simple_curve.order)
    e = random.randint(2, ecc.simple_curve.order)

    secret_key = (x, e)
    public_key = ecc.simple_curve.generator.to_jacobian_coordinates() * x
    print("Secret: {}\t\tPublic Key: {}".format(secret_key, public_key))
    chameleon = ecc_chameleon_hash(ecc.simple_curve, public_key)

    message = "This is my message"
    digest = chameleon.digest(message)
    print(digest)

    print(chameleon.verify(digest, message))



if __name__ == "__main__":
    test_chameleon_hash()
