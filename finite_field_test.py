import unittest
from finite_field import *

class finite_field_test(unittest.TestCase):

    def test_init(self):
        ff = finite_field(2)
        self.assertIsNotNone(ff)

    def test_element(self):
        ff = finite_field(2)
        element = ff(3)
        self.assertIsInstance(element, finite_field_element)
        self.assertEqual(element.e, 1)

    def test_sum(self):
        ff = finite_field(2)
        element_1 = ff(3)
        element_2 = ff(4)
        self.assertIsInstance(element_1, finite_field_element)
        self.assertEqual(element_1.e, 1)
        self.assertIsInstance(element_2, finite_field_element)
        self.assertEqual(element_2.e, 0)

        element_sum = element_1 + element_2
        self.assertIsInstance(element_sum, finite_field_element)
        self.assertEqual(element_sum.e, 1)
        self.assertEqual(element_sum.finite_field, ff)

    def test_subtraction(self):
        ff = finite_field(2)
        element_1 = ff(3)
        element_2 = ff(4)
        self.assertIsInstance(element_1, finite_field_element)
        self.assertEqual(element_1.e, 1)
        self.assertIsInstance(element_2, finite_field_element)
        self.assertEqual(element_2.e, 0)

        element_subtraction = element_1 - element_2
        self.assertIsInstance(element_subtraction, finite_field_element)
        self.assertEqual(element_subtraction.e, 1)
        self.assertEqual(element_subtraction.finite_field, ff)

    def test_product(self):
        ff = finite_field(5)
        element_1 = ff(3)
        element_2 = ff(4)
        self.assertIsInstance(element_1, finite_field_element)
        self.assertEqual(element_1.e, 3)
        self.assertIsInstance(element_2, finite_field_element)
        self.assertEqual(element_2.e, 4)

        element_subtraction = element_1 * element_2
        self.assertIsInstance(element_subtraction, finite_field_element)
        self.assertEqual(element_subtraction.e, 2)
        self.assertEqual(element_subtraction.finite_field, ff)

    def test_inverse(self):
        ff = finite_field(5)
        element_1 = ff(3)

        self.assertIsInstance(element_1, finite_field_element)
        self.assertEqual(element_1.e, 3)

        inverse = element_1.inverse()
        self.assertIsInstance(inverse, finite_field_element)
        self.assertEqual(inverse.e, 2)
        self.assertEqual(inverse.finite_field, ff)

    def test_division(self):
        ff = finite_field(5)
        element_1 = ff(3)
        element_2 = ff(4)
        self.assertIsInstance(element_1, finite_field_element)
        self.assertEqual(element_1.e, 3)
        self.assertIsInstance(element_2, finite_field_element)
        self.assertEqual(element_2.e, 4)

        element_division = element_1 / element_2
        self.assertIsInstance(element_division, finite_field_element)
        self.assertEqual(element_division.e, 2)
        self.assertEqual(element_division.finite_field, ff)

class polinomial_test(unittest.TestCase):

    def test_init(self):
        coeffs = {1: 1, 2: 2}
        pol = polynomial(coeffs)
        self.assertIsNotNone(pol)

    def test_sum(self):
        coeffs = {1: 1, 2: 2}
        pol_1 = polynomial(coeffs)
        pol_2 = polynomial(coeffs)

        pol_sum = pol_1 + pol_2
        self.assertIsInstance(pol_sum, polynomial)
        sum_coeffs = {1: 2, 2: 4}
        self.assertDictEqual(pol_sum.coeffs, sum_coeffs)

    def test_subtraction(self):
        coeffs = {1: 1, 2: 2}
        pol_1 = polynomial(coeffs)
        pol_2 = polynomial(coeffs)

        pol_subtraction = pol_1 - pol_2
        self.assertIsInstance(pol_subtraction, polynomial)
        self.assertTrue(pol_subtraction.is_zero())

    def test_product(self):
        coeffs = {1: 1, 2: 2}
        pol_1 = polynomial(coeffs)
        pol_2 = polynomial(coeffs)

        pol_product = pol_1 * pol_2
        self.assertIsInstance(pol_product, polynomial)
        prod_coeffs = {2: 1, 3: 4, 4: 4}
        self.assertDictEqual(pol_product.coeffs, prod_coeffs)

    def test_division(self):
        prod_coeffs = {2: 1, 3: 4, 4: 4}
        pol_1 = polynomial(prod_coeffs)
        print(prod_coeffs)
        coeffs = {1: 1, 2: 2}
        pol_2 = polynomial(coeffs)

        pol_division = pol_1 / pol_2
        self.assertIsInstance(pol_division, polynomial)
        self.assertDictEqual(pol_division.coeffs, coeffs)

    def test_mod(self):
        prod_coeffs = {1: 1, 2: 1, 3: 4, 4: 4}
        pol_1 = polynomial(prod_coeffs)
        coeffs = {1: 1, 2: 2}
        pol_2 = polynomial(coeffs)

        pol_mod = pol_1 % pol_2
        self.assertIsInstance(pol_mod, polynomial)
        mod_coeffs = {1: 1}
        self.assertDictEqual(pol_mod.coeffs, mod_coeffs)

    def test_pow(self):
        coeffs = {1: 1, 2: 2}
        pol_1 = polynomial(coeffs)

        pol_pow = pol_1 ** 2
        self.assertIsInstance(pol_pow, polynomial)
        pow_coeffs = {2: 1, 4: 2}
        self.assertDictEqual(pol_pow.coeffs, pow_coeffs)

class polynomial_finite_field_test(unittest.TestCase):

    def test_init(self):
        ff = finite_field(2)
        pol = polynomial([ff(2), ff(2), ff(1)])
        zero = polynomial([ff(0)])
        one = polynomial([ff(1)])
        ff_pol = finite_field(pol, zero, one)
        self.assertIsNotNone(ff_pol)

    def test_element(self):
        ff = finite_field(2)
        pol = polynomial([ff(2), ff(2), ff(1)])
        zero = polynomial([ff(0)])
        one = polynomial([ff(1)])
        ff_pol = finite_field(pol, zero, one)

        pol = ff_pol(polynomial([ff(1), ff(1)]))

        self.assertIsInstance(pol, finite_field_element)
        self.assertListEqual(pol.e.coeffs, [ff(1), ff(1)])

    def test_sum(self):
        ff = finite_field(3)
        pol = polynomial([ff(2), ff(2), ff(1)])
        zero = polynomial([ff(0)])
        one = polynomial([ff(1)])
        ff_pol = finite_field(pol, zero, one)

        pol_1 = ff_pol(polynomial([ff(1), ff(1)]))
        pol_2 = ff_pol(polynomial([ff(0), ff(1)]))

        pol_sum = pol_1 + pol_2

        self.assertIsInstance(pol_sum, finite_field_element)
        self.assertListEqual(pol_sum.e.coeffs, [ff(1), ff(2)])

    def test_subtraction(self):
        ff = finite_field(3)
        pol = polynomial([ff(2), ff(2), ff(1)])
        zero = polynomial([ff(0)])
        one = polynomial([ff(1)])
        ff_pol = finite_field(pol, zero, one)

        pol_1 = ff_pol(polynomial([ff(1), ff(1)]))
        pol_2 = ff_pol(polynomial([ff(0), ff(1)]))

        pol_subtraction = pol_1 - pol_2

        self.assertIsInstance(pol_subtraction, finite_field_element)
        self.assertListEqual(pol_subtraction.e.coeffs, [ff(1)])

    def test_inverse(self):
        ff = finite_field(3)
        pol = polynomial([ff(2), ff(2), ff(1)])
        zero = polynomial([ff(0)])
        one = polynomial([ff(1)])
        ff_pol = finite_field(pol, zero, one)

        pol_1 = ff_pol(polynomial([ff(1), ff(1)]))

        pol_inverse = pol_1.inverse()

        self.assertIsInstance(pol_inverse, finite_field_element)
        self.assertListEqual(pol_inverse.e.coeffs, [ff(2), ff(2)])


    def test_product(self):
        ff = finite_field(3)
        pol = polynomial([ff(2), ff(2), ff(1)])
        zero = polynomial([ff(0)])
        one = polynomial([ff(1)])
        ff_pol = finite_field(pol, zero, one)

        pol_1 = ff_pol(polynomial([ff(1), ff(1)]))
        pol_2 = ff_pol(polynomial([ff(0), ff(1)]))

        pol_product = pol_1 * pol_2

        self.assertIsInstance(pol_product, finite_field_element)
        self.assertListEqual(pol_product.e.coeffs, [ff(1), ff(2)])

    def test_division(self):
        ff = finite_field(3)
        pol = polynomial([ff(2), ff(2), ff(1)])
        zero = polynomial([ff(0)])
        one = polynomial([ff(1)])
        ff_pol = finite_field(pol, zero, one)

        pol_1 = ff_pol(polynomial([ff(1), ff(2)]))
        pol_2 = ff_pol(polynomial([ff(1), ff(1)]))

        pol_division = pol_1 / pol_2

        self.assertIsInstance(pol_division, finite_field_element)
        self.assertListEqual(pol_division.e.coeffs, [ff(0), ff(1)])

    def test_pow(self):
        ff = finite_field(3)
        pol = polynomial([ff(2), ff(2), ff(1)])
        zero = polynomial([ff(0)])
        one = polynomial([ff(1)])
        ff_pol = finite_field(pol, zero, one)

        pol = ff_pol(polynomial([ff(1), ff(2)]))

        pol_pow = pol ** 3
        self.assertIsInstance(pol_pow, finite_field_element)
        self.assertListEqual(pol_pow.e.coeffs, [ff(0), ff(1)])

    def test_is_irreducible(self):
        ff = finite_field(3)
        pol = polynomial([ff(2), ff(2), ff(1)])

        self.assertTrue(pol.is_irreducible())

if __name__ == "__main__":
    unittest.main()
