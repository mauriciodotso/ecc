# ECC #

* Pure implementation of ECC in python
* Implementation of Chameleon Hash using ECC in Python as well

### How do I get set up? ###

* Just import the file ecc.py if you want to use ECC in python
* Just import the file chameleon_hash.py if you want to use Chameleon Hashs in python
* This libraries do not have any dependency that does not already comes with the default python interpreter

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* mauricio.so "at" inf.ufsc.br